    package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.renderscript.ScriptGroup
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.example.myapplication.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.frm_alerta.*
import kotlinx.android.synthetic.main.frm_alerta.view.*
import kotlinx.android.synthetic.main.activity_main.view.txtDescripcion as txtDescripcion1
import kotlinx.android.synthetic.main.activity_main.view.txtNombre as txtNombre1

    class MainActivity : AppCompatActivity() , IListas {
        private lateinit var binding : ActivityMainBinding
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

            binding = ActivityMainBinding.inflate(layoutInflater)
            setContentView(binding.root)

            val marcas=listOf("Hyundai","Nissan","Volvo","Kia","Ferrari")
            val adaptador = ArrayAdapter(this,  R.layout.stilo_espinner, marcas)
            spMarca.adapter=adaptador

            binding.btnMostrar.setOnClickListener(){
            var nombre:String=txtNombre.text.toString()
                var marca:String=spMarca.selectedItem.toString()
                var anio =binding.txtAnio.text.toString().toInt()
                var descripcion=binding.txtDescripcion.text.toString()

                var auto:Auto=Auto(nombre,marca,anio,descripcion )
                createLoginDialogo(auto).show()
            }
        }

        override fun createLoginDialogo(auto: Auto): AlertDialog {
            val alertDialog:AlertDialog
            val builder=AlertDialog.Builder(this)
            val inflater = layoutInflater
            val vista : View =inflater.inflate(R.layout.frm_alerta,null)
            builder.setView(vista)

            auto?.let {
                vista.txtTitular.text=it.nombre
                vista.txtMarca.text=it.marca
                vista.txtAnioAlerta.text=it.anio.toString()
                vista.txtDescripcion.text=it.descripcion
            }

            alertDialog=builder.create()
            val boton=vista.findViewById<Button>(R.id.btnSi)
            boton.setOnClickListener {
                alertDialog.dismiss()
            }
            return  alertDialog
        }


    }